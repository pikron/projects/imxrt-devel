# imxRT Development and Experiments (NuttX, Teensy-4, OMK and more)

This project is playground to test suitability of imxRT
to be used as s base of next generation of middle class (MCU,
but OS based) embedded application.

The project has been founded by [Pavel Pisa](https://www.openhub.net/accounts/ppisa)
and Petr Porazl from [PiKRON](https://pikron.com/) company.
The [PJRC](https://www.pjrc.com/) [Teensy-4.1](https://www.pjrc.com/store/teensy41.html)
imxRT based module has been selected as the first target. Petr Porazil designed
[BaseBoard](https://gitlab.com/pikron/projects/imxrt-devel/-/wikis/teensy_bb)
(actual version 1) for Teensy-4.1 to provide CAN, serial port, RS-485, SPI interfaces,
SPI LCD based graphics and PMSM/BLDC/DC/stepper-motor vector control motion control
peripherals. The DIN rail enclosure is his work as well.

The [NuttX](https://github.com/apache/incubator-nuttx) operating system
has been ported to the Teensy-4.1 by Mihal Lenc working in the frame
of [Open Technologies Research Education and Exchange Services](https://gitlab.fel.cvut.cz/otrees)
at the [Department of Control Engineering](https://control.fel.cvut.cz/).

## Licenses

- NuttX is licensed under Apache License, Version 2.0
- PiKRON BaseBoard design is licensed under MIT license
- OMK build system is lecesed under GNU General Public License
- The example application [`appfoo`](nuttx/apps-omk/appfoo), [`libbar`](nuttx/apps-omk/libbar) and [`wrapup`](nuttx/apps-omk/wrapup) in [`apps-omk`](nuttx/apps-omk) are provided under Public Domain principle

## PiKRON BaseBoard v1 for Teensy-4.1

Detailed description at project [Wiki](https://gitlab.com/pikron/projects/imxrt-devel/-/wikis/teensy_bb).

### External Connectors

- MicroUSB device cnnection on Teensy-4.1 
- CN2 - GPIO (AD_B0_03, AD_B0_02) alternative to CAN2 on CN12 if JP1 not set
- CN4 - UART1 (3V3 RXD, TXD)
- CN5 - GPIO (AD_B1_07, AD_B1_06, I2C3)
- CN6 - MOT_PWM (PMSM, stepper, DC motor up to 4 phase, 50 A output)
- CN7 - POWER (9 to 35 VDC, separated logic and motor power pins)
- CN8 - RS-485/uLAN UART2 (galvanically isolated, JP7 1-2 connected, shared with CN9)
- CN9 - RS232 UART2 (galvanically isolated, JP7 2-3 connected, shared with CN8)
- CN10 - CAN3 (CAN FD, galvanically isolated)
- CN11 - CAN1 (not isolated)
- CN12 - CAN2 (not isolated, JP1 connected, shared with CN2 GPIO)
- CN13 - USB Host
- CN15 - Gigabit ETHERNET
- CN17 - TBUS_CAN3 (DIN rail T-Bus connector to backplane with CAN3 CAN FD signals)
- CN19 - IRC (Incremental Encoder Input, complementar A, B, I, M)

### SPI Connected LCD Display

- resolution 240 x 320
- When unused SPI signals can be used externaly though CN3 connector

### Rotary Encoder for User Input

- connected to pins B1_13 B1_12

## Build NuttX System for BaseBoard v1 Use

The first ensure that you have `kconfig-frontends` package available on the system. Install from the distribution

```
sudo apt install kconfig-frontends
```
or build it from sources.

Check that you have installed `gcc-arm-none-eabi` and `genromfs` (can be used for compressed disk images builds).

You need Teensy programming application [`teensy_loader_cli`](https://github.com/PaulStoffregen/teensy_loader_cli) availbel on the host system.

Then clone and build application

```
# clone repository
git clone https://gitlab.com/pikron/projects/imxrt-devel.git
# clone or update nuttx and NuttX provided app repositories
# to the version versioned in the target application 
git submodule update --init
# enter NuttX variant of the firmware
cd nuttx
# build NuttX base system and companion and custom applications
make
```

This top level make descends to NuttX subdirectory `nuttx/nuttx` and configures system

```
tools/configure.sh teensy-4.x:pikron-bb
```

The actual configuration is for NuttX shell NSH on Teensy-4.1 connected to PiKRON BaseBoard v1. The actual BSP directory is [`nuttx/arm/imxrt/teensy-4.x`](https://github.com/apache/incubator-nuttx/tree/master/boards/arm/imxrt/teensy-4.x).

Then regular `make` is run in NuttX directory, it compiles NuttX shell and other utilities in companion `nuttx-apps` directory `../apps`.

Then `make export` is run in the NuttX tree and generated zip is extracted in `nuttx/nuttx-export` directory. This directory contains NuttX standalone link kit which allows to build applications without need to consult rest of `nuttx` and `apps` sources for given BSP and configuration.

Then top level `Makefile` enters OMK build applications directory, `apps-omk` in our case. They are controlled by [OMK rules](https://gitlab.com/pikron/projects/imxrt-devel/-/wikis/omk-manual) system and this is only part of the tree versioned directly in the application repository.

You can use included `nuttx/load-nuttx` to load plain NuttX system on the target board or `nuttx/load-wrapup` script which loads NuttX combined with all applications compiled in `nuttx/apps-omk` build.

```
teensy_loader_cli --mcu=TEENSY41 -v -w apps-omk/_compiled/bin/start.hex
```

You should see next output on the console

```
romdisk_register(0, 0x6002f032, 16, 64)
Mounting ROMFS filesystem at target=/etc with source=/dev/ram0
Starting OMK template provided rcS
start application called
Starting user application
start [5:100]

NuttShell (NSH) NuttX-3.6.1
nsh>
```

The default start attempts to obtain IP address over DHCP and it causes some delay before it timeouts if ETHERNET is not connected. Disable of the ETHERNET driver in the NuttX build if the ETHERNET connection is not planned.

You can interact with shell. You can check for applications build in shell. Use `help` command

```
nsh> help
help usage:  help [-v] [<cmd>]

  .         cd        exec      ifdown    mkdir     nslookup  set       umount
  [         cp        exit      ifup      mkfatfs   ps        sleep     unset
  ?         cmp       false     insmod    mkfifo    put       source    usleep
  addroute  dirname   free      kill      mkrd      pwd       test      xd
  arp       dd        get       losetup   mh        rm        telnetd
  basename  delroute  help      ls        mount     rmdir     time
  break     df        hexdump   lsmod     mv        rmmod     true
  cat       echo      ifconfig  mb        mw        route     uname

Builtin Apps:
  appfoo     ping6      renew      sh         ntpcstop   cansend
  start      ntpcstart  tee        candump    ping       nsh
```

You can see start script

```
cat /etc/init.d/rcS
```

The output is

```
# Create a RAMDISK and mount it at /tmp

echo Starting OMK template provided rcS

# use if binfs is compiled in
#mkdir /bin
#mount -t binfs binfs /bin
#set PATH /bin

mount -t tmpfs tmpfs /tmp

echo Starting user application
start
```

More lines are commented out, but at the end you see that build-in `start` application is started at boot. It is empty in this case so it is spawned without ``&``.

You can modify this or others files which appears under `/etc` at the location `nuttx/apps-omk/wrapup/romfs/init.d/rcS`. Wrapup application with start command sources are located at `nuttx/apps-omk/wrapup`.

The `Makefile` in this directory is only proxy which locates `Makefile.rules` file in the top of standalone applications three and then interprets `nuttx/apps-omk/wrapup/Makefile.omk`:

```
lib_LIBRARIES =

include_HEADERS =

bin_PROGRAMS = start

start_SOURCES = start.c
start_SOURCES += nsh_romfsetc.c
start_PROGBUILTIN = all
start_PROGBUILTIN_EXCLUDE = module
start_KMODBUILTIN = all
start_EMBEDROMFS = romfs
```

This file specifies, that there is only single target application in the directory

```
bin_PROGRAMS = start
```

Then there is list of the sources build into this binary

```
start_SOURCES =  a.c b.c c.c
start_SOURCES +=  d.c e.c f.c
``` 
Then it is specifies that `all` build in commands found in NuttX `apps` and OMK build should be included into target NuttX image.

```
start_KMODBUILTIN = all
```

Then it is specified that `romfs` directory should be packaged and provided as `/etc` content in the target application.

There are no library build and exported by this directory

```
lib_LIBRARIES =
```

and no header files are published

```
include_HEADERS =
```

More examples how can be controlled build of applications, libraries and kernel modules for NuttX by OMK system is provided in the [`nuttx-devel`](https://github.com/ppisa/nuttx-devel) [`nuttx-omk-template`](https://github.com/ppisa/nuttx-devel/tree/master/nuttx-omk-template) directory.

Description of NuttX specific defines in OMK build can be found there in the [`Makefile.rules`](https://github.com/ppisa/nuttx-devel/blob/4a1d6855459adf98ca432570c1405d17a3695a01/nuttx-omk-template/Makefile.rules#L348) file.

The more complex custom NuttX applications, bundles and even kernel drivers build can be found as part of [uLAN](http://ulan.sourceforge.net/) project. The toplevel of NuttX build [there](https://gitlab.com/pikron/sw-base/ulan/-/tree/master/nuttx). Driver build for system-less, Windows, Linux and NuttX example [`Makefile.omk`](https://gitlab.com/pikron/sw-base/ulan-drv/-/blob/master/ul_drv/Makefile.omk) file is part of [`ul_drv`](https://gitlab.com/pikron/sw-base/ulan-drv/-/tree/master/ul_drv). Only Windows build requires separate makefile.

The example configuration includes export of all libc, libm and syscalls symbols. So it is possible to load external applications over TFTP

```
get -b -f /tmp/apptftp -h 10.0.0.158 nuttx/imxrt/appfoo.elf
/tmp/apptftp
```
